import tape from "tape";
import {IncludeNode} from "../../src/nodes/include.node";
import {TwingCompiler, TwingEnvironment, TwingLoaderNull, TwingNodeExpressionConstant, TwingNodeInclude} from "twing";

tape('IncludeNode', ({test}) => {
    test('', ({same, end}) => {
        const node = new IncludeNode(
            new TwingNodeInclude(
                new TwingNodeExpressionConstant('a', 0, 0),
                new TwingNodeExpressionConstant('b', 0, 0),
                false, false, 0, 0
            )
        )

        const compiler = new TwingCompiler(
            new TwingEnvironment(
                new TwingLoaderNull()
            )
        );

        compiler.compile(node);

        same(compiler.getSource(), 'outputBuffer.echo(await this.include(context, outputBuffer, await import(`a`), `b`, true, false, 0));\n');

        end();
    });
});
import {TwingNodeInclude} from "twing";
import type {TwingCompiler} from "twing";

export class IncludeNode extends TwingNodeInclude {
    constructor(node: TwingNodeInclude) {
        super(
            node.getNode('expr'),
            node.getNode('variables'),
            node.getAttribute('only'),
            node.getAttribute('ignore_missing'),
            node.getTemplateLine(),
            node.getTemplateColumn()
        );
    }

    addGetTemplate(compiler: TwingCompiler) {
        compiler
            .raw('await import(')
            .subcompile(this.getNode('expr'))
            .raw(')');
    }
}
import {TwingNodeModule} from "twing";
import type {TwingCompiler} from "twing";

export class ModuleNode extends TwingNodeModule {
    constructor(node: TwingNodeModule) {
        super(
            node.getNode("body"),
            node.hasNode("parent") ? node.getNode("parent") : null as any, // todo: remove once Twing is fixed
            node.getNode("blocks"),
            node.getNode("macros"),
            node.getNode("traits"),
            node.getAttribute("embedded_templates"),
            node.source
        );
    }

    compileDoGetParent(compiler: TwingCompiler) {
        if (this.hasNode("parent")) {
            const node = this.getNode("parent");
            compiler.write(`doGetParent(context) {\n`)
                .indent()
                .write(`return import("${node.getAttribute("value")}").then((parent) => {this.parent = parent; return parent;});\n`)
                .outdent()
                .write('}\n\n');
        }
    }
}
import {TwingBaseNodeVisitor, TwingNode, TwingNodeInclude, TwingNodeModule} from "twing";
import {ModuleNode} from "./nodes/module.node.js";
import {IncludeNode} from "./nodes/include.node.js";

export class Visitor extends TwingBaseNodeVisitor {
    doEnterNode(node: TwingNode) {
        return node;
    }

    doLeaveNode(node: TwingNode) {
        if (node instanceof TwingNodeModule) {
            node = new ModuleNode(node);
        }

        if (node instanceof TwingNodeInclude) {
            node = new IncludeNode(node);
        }

        return node;
    }

    getPriority() {
        return 255;
    }
}

import type {Plugin, TransformHook} from "rollup";
import type {FilterPattern} from "@rollup/pluginutils";
import {createFilter} from "@rollup/pluginutils";
import {extname} from "path";
import {
    TwingEnvironment,
    TwingLoaderArray,
    TwingLoaderFilesystem,
    TwingLoaderRelativeFilesystem,
    TwingSource
} from "twing";
import {Visitor} from "./visitor.js";

const createTwigPlugin: (options: {
    include?: FilterPattern,
    exclude?: FilterPattern,
}) => Plugin = (options = {}) => {
    const filter = createFilter(options.include, options.exclude);
    const extensions = ['.twig'];

    const transform: TransformHook = (
        code,
        id
    ) => {
        if (!filter(id) || !extensions.includes(extname(id))) {
            return;
        }

        const environment = new TwingEnvironment(
            new TwingLoaderArray([
                new TwingLoaderFilesystem(),
                new TwingLoaderRelativeFilesystem()
            ])
        );

        environment.addNodeVisitor(new Visitor());

        const source = new TwingSource(code, id);
        const output = environment.compileSource(source);

        return {
            code: `import {TwingTemplate, TwingEnvironment, TwingLoaderNull} from 'twing';

const environment = new TwingEnvironment(new TwingLoaderNull());

const module = {};

${output}

const classes = module.exports(TwingTemplate);
const Template = classes.get(0);
const template = new Template(environment);

export const display = template.display.bind(template);
export const render = template.render.bind(template);
export const source = template.source;
`,
            map: {
                mappings: ''
            }
        }
    };

    return {
        name: 'twig',
        transform
    };
};

export default createTwigPlugin;